#include "Console.h"
#include "ui_Console.h"

#define SOL_ALL_SAFETIES_ON 1
#include "sol/sol.hpp"

Console::Console(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::Console)
{
    ui->setupUi(this);

    connect(ui->runButton, &QPushButton::clicked, this, &Console::run);
}

Console::~Console()
{
    delete ui;
}

void Console::run() const
{
    // init lua
    sol::state lua;
    lua.open_libraries(sol::lib::base);

    QStringList outputStrings;
    QStringList errorStrings;

    // override the "print" function to capture the script output
    lua.set_function("print", [&lua, &outputStrings](sol::variadic_args args) {
        for (auto arg: args) {
            auto ba = QByteArray::fromStdString(lua["tostring"](arg.get<sol::object>()).get<std::string>());
            outputStrings << ba;
        }
    });

    // read script from input box, ignore formatting, etc.
    auto script = ui->inputBox->toPlainText();

    auto result = lua.safe_script(script.toStdString(), [&errorStrings](lua_State*, sol::protected_function_result pfr) {
        // error occured during script execution - print it
        sol::error err = pfr;
        errorStrings << tr("Script error");
        errorStrings << err.what();

        // return the protected_function_result
        return pfr;
    });

    // show errors if any, output otherwise
    if (errorStrings.isEmpty()) {
        ui->outputBox->setPlainText(outputStrings.join("\n"));
    } else {
        ui->outputBox->setPlainText(errorStrings.join("\n"));
    }
}

