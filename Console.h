#pragma once

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui {
class Console;
}
QT_END_NAMESPACE

class Console : public QMainWindow
{
    Q_OBJECT

public:
    Console(QWidget *parent = nullptr);
    ~Console();

public slots:
    void run() const;

private:
    Ui::Console *ui;
};
